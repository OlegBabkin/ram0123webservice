package com.practice.olegbabkin.WebServicePractice;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import com.practice.olegbabkin.WebServicePractice.domain.City;
import com.practice.olegbabkin.WebServicePractice.domain.Country;
import com.practice.olegbabkin.WebServicePractice.domain.Role;
import com.practice.olegbabkin.WebServicePractice.domain.User;
import com.practice.olegbabkin.WebServicePractice.service.CityService;
import com.practice.olegbabkin.WebServicePractice.service.CountryService;
import com.practice.olegbabkin.WebServicePractice.service.RoleService;
import com.practice.olegbabkin.WebServicePractice.service.UserService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebServicePracticeApplication.class)
@WebAppConfiguration
public class WebServicePracticeControllersTests {

	private static final String COUNTRY_PATH = "/api/world/country";
	private static final String CITY_PATH = "/api/world/city";

	private final MediaType contentType = new MediaType(
		MediaType.APPLICATION_JSON.getType(), 
		MediaType.APPLICATION_JSON.getSubtype(), 
		Charset.forName("utf8"));
	
	private MockMvc mockMvc;

	private HttpMessageConverter mappingJackson2HttpMessageConverter;

	private final MockDataSource mds = new MockDataSource();

	@Autowired
	private CountryService countryService;

	@Autowired
	private CityService cityService;

	@Autowired
	private UserService userService;

	@Autowired
	private RoleService roleService;

	private User mockUser;
	private User mockAdmin;

	@Value("${security.jwt.client-id}")
	private String clientId;
	
	@Value("${security.jwt.client-secret}")
    private String clientSecret;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {
		this.mappingJackson2HttpMessageConverter = Arrays.asList(converters)
			.stream()
			.filter(c -> c instanceof MappingJackson2HttpMessageConverter)
			.findAny()
			.orElse(null);

		assertNotNull("the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
	}

	@Before
	public void setUp() throws Exception {
		this.mockMvc = webAppContextSetup(webApplicationContext).apply(springSecurity()).build();
		this.cityService.deleteAll();
		this.countryService.deleteAll();
		this.userService.deleteAll();
		this.roleService.deleteAll();

		for (Country c : this.mds.getMockCountries()) {
			this.countryService.saveOne(c);
		}

		for (City c : this.mds.getMockCities()) {
			this.cityService.saveOne(c);
		}

		for (Role r : this.mds.getMockRoles()) {
			this.roleService.saveOne(r);
		}

		for (User u : this.mds.getMockUsers()) {
			if (u.getId() == 1) {
				mockAdmin = this.userService.addUser(u);
			}
			if (u.getId() == 2) {
				mockUser = this.userService.addUser(u);
			}
		}
	}
	/***************************** CountryController tests *****************************/
	@Test
	public void shouldReturnAllCountries() throws Exception {
		mockMvc.perform(get(COUNTRY_PATH + "/all"))
			.andExpect(status().isOk())
			.andExpect(content().contentType(contentType))
			.andExpect(jsonPath("$", hasSize(6)))
			.andExpect(jsonPath("$[0].code", is(this.mds.getMockCountries().get(0).getCode())))
			.andExpect(jsonPath("$[0].capital", is(this.mds.getMockCountries().get(0).getCapital().intValue())))
			.andExpect(jsonPath("$[0].name", is(this.mds.getMockCountries().get(0).getName())))
			.andExpect(jsonPath("$[1].code", is(this.mds.getMockCountries().get(1).getCode())))
			.andExpect(jsonPath("$[1].capital", is(this.mds.getMockCountries().get(1).getCapital().intValue())))
			.andExpect(jsonPath("$[1].name", is(this.mds.getMockCountries().get(1).getName())));
	}

	@Test
	public void shouldReturnSingleCountry() throws Exception {
		System.out.println(loginAndGetAccessToken(mockUser.getUsername(), "test"));
		final String token = loginAndGetAccessToken(mockUser.getUsername(), "test");

		mockMvc.perform(get(COUNTRY_PATH + "/ttc").header("Authorization", "Bearer " + token))
			.andExpect(status().isOk())
			.andExpect(content().contentType(contentType))
			.andExpect(jsonPath("$.code", is(this.mds.getMockCountries().get(2).getCode())))
			.andExpect(jsonPath("$.capital", is(this.mds.getMockCountries().get(2).getCapital().intValue())))
			.andExpect(jsonPath("$.name", is(this.mds.getMockCountries().get(2).getName())));
	}

	@Test
	public void shouldReturnSingleCountryNotFound() throws Exception {
		mockMvc.perform(get(COUNTRY_PATH + "/tty"))
			.andExpect(status().isNotFound());
	}

	@Test
	public void shouldReturnCountriesByContinent() throws Exception {
		mockMvc.perform(get(COUNTRY_PATH + "/continent/continent1"))
			.andExpect(status().isOk())
			.andExpect(content().contentType(contentType))
			.andExpect(jsonPath("$", hasSize(2)))
			.andExpect(jsonPath("$[0].code", is(this.mds.getMockCountries().get(4).getCode())))
			.andExpect(jsonPath("$[0].capital", is(this.mds.getMockCountries().get(4).getCapital().intValue())))
			.andExpect(jsonPath("$[0].name", is(this.mds.getMockCountries().get(4).getName())))
			.andExpect(jsonPath("$[1].code", is(this.mds.getMockCountries().get(5).getCode())))
			.andExpect(jsonPath("$[1].capital", is(this.mds.getMockCountries().get(5).getCapital().intValue())))
			.andExpect(jsonPath("$[1].name", is(this.mds.getMockCountries().get(5).getName())));
	}

	@Test
	public void shouldReturnCountriesByContinentNoContent() throws Exception {
		mockMvc.perform(get(COUNTRY_PATH + "/continent/continent2"))
			.andExpect(status().isNoContent());
	}

	@Test
	public void shouldReturnCitiesByCountry() throws Exception {
		mockMvc.perform(get(COUNTRY_PATH + "/" + mds.getMockCountries().get(0).getName() + "/city/all"))
			.andExpect(status().isOk())
			.andExpect(content().contentType(contentType))
			.andExpect(jsonPath("$", hasSize(5)))
			.andExpect(jsonPath("$[0].id", is(this.mds.getMockCities().get(0).getId().intValue())))
			.andExpect(jsonPath("$[0].country.code", is(this.mds.getMockCities().get(0).getCountry().getCode())))
			.andExpect(jsonPath("$[0].name", is(this.mds.getMockCities().get(0).getName())))
			.andExpect(jsonPath("$[1].id", is(this.mds.getMockCities().get(6).getId().intValue())))
			.andExpect(jsonPath("$[1].country.code", is(this.mds.getMockCities().get(6).getCountry().getCode())))
			.andExpect(jsonPath("$[1].name", is(this.mds.getMockCities().get(6).getName())));
	}

	@Test
	public void shouldReturnCitiesByCountryNotFound() throws Exception {
		mockMvc.perform(get(COUNTRY_PATH + "/" + "name" + "/city/all"))
			.andExpect(status().isNotFound());
	}

	@Test
	public void shouldReturnTop10CountriesByPopulationLessThan() throws Exception {
		mockMvc.perform(get(COUNTRY_PATH + "/top10/population/lessthan/" + 1000000))
			.andExpect(status().isOk())
			.andExpect(content().contentType(contentType))
			.andExpect(jsonPath("$", hasSize(5)))
			.andExpect(jsonPath("$[0].population", is(this.mds.getMockCountries().get(4).getPopulation().intValue())))
			.andExpect(jsonPath("$[1].population", is(this.mds.getMockCountries().get(3).getPopulation().intValue())))
			.andExpect(jsonPath("$[2].population", is(this.mds.getMockCountries().get(2).getPopulation().intValue())))
			.andExpect(jsonPath("$[3].population", is(this.mds.getMockCountries().get(1).getPopulation().intValue())))
			.andExpect(jsonPath("$[4].population", is(this.mds.getMockCountries().get(0).getPopulation().intValue())));
	}

	@Test
	public void shouldReturnTop10CountriesByPopulationLessThanNoContent() throws Exception {
		mockMvc.perform(get(COUNTRY_PATH + "/top10/population/lessthan/" + 100000))
			.andExpect(status().isNoContent());
	}

	@Test
	public void shouldAddCountryAndReturn() throws Exception {
		Country newCountry = new Country();
		newCountry.setCode("TTT");
		newCountry.setName("Country name test");
		newCountry.setContinent("continent");
		newCountry.setRegion("region");
		newCountry.setSurfacearea(100000);
		newCountry.setIndepyear(1900);
		newCountry.setPopulation(600000);
		newCountry.setLifeexpectancy(56.0);
		newCountry.setGnp(100000.0);
		newCountry.setGnpold(110000.0);
		newCountry.setLocalname("Country name test");
		newCountry.setGovernmentform("governmentform");
		newCountry.setHeadofstate("HeadOfState");
		newCountry.setCode2("TT");
		String countryJson = json(newCountry);

		mockMvc.perform(post(COUNTRY_PATH + "/add")
			.contentType(contentType)
			.content(countryJson))
			.andExpect(status().isCreated())
			.andExpect(jsonPath("$.code", is(newCountry.getCode())));
	}

	@Test
	public void shouldUpdateCountryAndReturn() throws Exception {
		Country country = countryService.getOne("TTA");
		country.setName("Updated");

		String countryJson = json(country);

		mockMvc.perform(put(COUNTRY_PATH + "/tta/update")
			.contentType(contentType)
			.content(countryJson))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.name", is("Updated")));
	}

	// Helps to evaluate problem with cascade deleting
	@Test
	public void shouldDeleteCountry() throws Exception {
		mockMvc.perform(delete(COUNTRY_PATH + "/ttf")
			.contentType(contentType))
			.andExpect(status().isOk());
	}

	@Test
	public void shouldDeleteCountryNotFound() throws Exception {
		mockMvc.perform(delete(COUNTRY_PATH + "/tty")
			.contentType(contentType))
			.andExpect(status().isNotFound());
	}

	/***************************** CityController tests *****************************/
	@Test
	public void shouldReturnAllCitiesWithCountryRelation() throws Exception {
		mockMvc.perform(get(CITY_PATH + "/all"))
			.andExpect(status().isOk())
			.andExpect(content().contentType(contentType))
			.andExpect(jsonPath("$", hasSize(10)))
			.andExpect(jsonPath("$[0].id", is(this.mds.getMockCities().get(0).getId().intValue())))
			.andExpect(jsonPath("$[0].country.code", is(this.mds.getMockCities().get(0).getCountry().getCode())))
			.andExpect(jsonPath("$[0].name", is(this.mds.getMockCities().get(0).getName())))
			.andExpect(jsonPath("$[1].id", is(this.mds.getMockCities().get(1).getId().intValue())))
			.andExpect(jsonPath("$[1].country.code", is(this.mds.getMockCities().get(1).getCountry().getCode())))
			.andExpect(jsonPath("$[1].name", is(this.mds.getMockCities().get(1).getName())));
	}

	@Test
	public void shouldReturnSingleCity() throws Exception {
		mockMvc.perform(get(CITY_PATH + "/3"))
			.andExpect(status().isOk())
			.andExpect(content().contentType(contentType))
			.andExpect(jsonPath("$.id", is(this.mds.getMockCities().get(2).getId().intValue())))
			.andExpect(jsonPath("$.country.code", is(this.mds.getMockCities().get(2).getCountry().getCode())))
			.andExpect(jsonPath("$.name", is(this.mds.getMockCities().get(2).getName())));
	}

	@Test
	public void shouldReturnSingleCityNotFound() throws Exception {
		mockMvc.perform(get(CITY_PATH + "/11"))
			.andExpect(status().isNotFound());
	}

	@Test
	public void shouldReturnCitiesContainsPartName() throws Exception {
		mockMvc.perform(get(CITY_PATH + "/name/part/differ"))
			.andExpect(status().isOk())
			.andExpect(content().contentType(contentType))
			.andExpect(jsonPath("$", hasSize(1)))
			.andExpect(jsonPath("$[0].id", is(this.mds.getMockCities().get(8).getId().intValue())))
			.andExpect(jsonPath("$[0].name", is(this.mds.getMockCities().get(8).getName())));
	}

	@Test
	public void shouldReturnCitiesContainsPartNameNotFound() throws Exception {
		mockMvc.perform(get(CITY_PATH + "/name/part/noname"))
			.andExpect(status().isNotFound());
	}

	@Test
	public void shouldAddCityAndReturn() throws Exception {
		City newCity = new City();
		newCity.setId(12L);
		newCity.setName("New City");
		newCity.setCountry(countryService.getOne("TTB"));
		newCity.setDistrict("district");
		newCity.setPopulation(100001L);
		String countryJson = json(newCity);
		// System.out.println("========================================="+countryJson);
		mockMvc.perform(post(CITY_PATH + "/add")
			.contentType(contentType)
			.content(countryJson))
			.andExpect(status().isCreated())
			.andExpect(jsonPath("$.id", is(newCity.getId().intValue())))
			.andExpect(jsonPath("$.name", is(newCity.getName())))
			.andExpect(jsonPath("$.country.code", is(newCity.getCountry().getCode())));
	}

	@Test
	public void shouldUpdateCityAndReturn() throws Exception {
		City city = cityService.getOne(7L);
		city.setName("Updated");

		String cityJson = json(city);

		mockMvc.perform(put(CITY_PATH + "/7/update")
			.contentType(contentType)
			.content(cityJson))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.name", is("Updated")));
	}

	@Test
	public void shouldDeleteCity() throws Exception {
		mockMvc.perform(delete(CITY_PATH + "/6")
			.contentType(contentType))
			.andExpect(status().isOk());
	}

	@Test
	public void shouldDeleteCityNotFound() throws Exception {
		mockMvc.perform(delete(CITY_PATH + "/15")
			.contentType(contentType))
			.andExpect(status().isNotFound());
	}

	/****************************** Utils ***********************************/
	protected String json(Object object) throws IOException {
		MockHttpOutputMessage message = new MockHttpOutputMessage();
		this.mappingJackson2HttpMessageConverter.write(object, MediaType.APPLICATION_JSON, message);
		return message.getBodyAsString();
	}

	protected String loginAndGetAccessToken(String username, String password) throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
    	params.add("grant_type", "password");
    	//params.add("client_id", clientId);
    	params.add("username", username);
		params.add("password", password);
		
		System.out.println(httpBasic(clientId, clientSecret));
		ResultActions result = mockMvc.perform(post("/oauth/token")
			.params(params).with(httpBasic(clientId, clientSecret)).contentType("application/x-www-form-urlencoded")
			.accept(contentType))
			.andExpect(status().isOk())
			.andExpect(content().contentType(contentType));

		String resultJson = result.andReturn().getResponse().getContentAsString();
		System.out.println("**********************************************************" + resultJson);
		JacksonJsonParser jsonParser = new JacksonJsonParser();

		return jsonParser.parseMap(resultJson).get("access_token").toString();
	}
	
}
