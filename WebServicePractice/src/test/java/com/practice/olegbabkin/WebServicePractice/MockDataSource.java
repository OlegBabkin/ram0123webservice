package com.practice.olegbabkin.WebServicePractice;

import java.util.ArrayList;
import java.util.List;

import com.practice.olegbabkin.WebServicePractice.domain.City;
import com.practice.olegbabkin.WebServicePractice.domain.Country;
import com.practice.olegbabkin.WebServicePractice.domain.Role;
import com.practice.olegbabkin.WebServicePractice.domain.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class MockDataSource {

    private final String[] codes = {"TTA", "TTB", "TTC", "TTD", "TTE", "TTF"};
    private final String[] codes2 = {"TA", "TB", "TC", "TD", "TE", "TF"};
    private static final int CITIES_COUNT = 10;

    private final List<Country> mockCountries = new ArrayList<>();

    private final List<City> mockCities = new ArrayList<>();

    private final List<User> mockUsers = new ArrayList<>();

    private final List<Role> mockRoles = new ArrayList<>();

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    public MockDataSource() {
        passwordEncoder = new BCryptPasswordEncoder();
        setMockCountries();
        setMockCities();
        setMockRoles();
        setMockUsers();
    }

	private void setMockCountries() {
        
        for (int i = 0; i < codes.length; i++) {
            Country country = new Country();
            country.setCode(codes[i]);
            country.setName("Country name " + i);
            if (i > 3) country.setContinent("continent1");
            else country.setContinent("continent");
            if (i > 3) country.setRegion("region1");
            else country.setContinent("region");
            country.setSurfacearea(100000 + i * 50000);
            country.setIndepyear(1900 + i * 10);
            country.setPopulation(600000 + i * 100000);
            country.setLifeexpectancy(56 + i * 2.5);
            country.setGnp(100000.0);
            country.setGnpold(110000.0);
            country.setLocalname("Country name " + i);
            if (i > 3)country.setGovernmentform("governmentform1");
            else country.setGovernmentform("governmentform");
            country.setHeadofstate("HeadOfState" + i);
            country.setCapital(Long.valueOf(i + 1));
            country.setCode2(codes2[i]);
            this.mockCountries.add(country);
        }

	}

	private void setMockCities() {

        for (int i = 0; i < CITIES_COUNT; i++) {
            City city = new City();
            city.setId(Long.valueOf(i + 1));
            if (i == 8) city.setName("City different name");
            else city.setName("City name " + i);
            if (i < codes.length) city.setCountry(mockCountries.get(i));
            else city.setCountry(mockCountries.get(0));
            city.setDistrict("district" + i);
            city.setPopulation(Long.valueOf(50000 + i * 20000));
            this.mockCities.add(city);
        }

    }
    
    private void setMockRoles() {
        this.mockRoles.add(new Role(1, "admin"));
        this.mockRoles.add(new Role(2, "user"));
    }

    private void setMockUsers() {
        User admin = new User(1, "admin@test.com", passwordEncoder.encode("test"));
        admin.setRoles(this.mockRoles);
        this.mockUsers.add(admin);

        User user = new User(2, "user@test.com", passwordEncoder.encode("test"));
        List<Role> roles = new ArrayList<>();
        roles.add(this.mockRoles.get(1));
        user.setRoles(roles);
        this.mockUsers.add(user);
    }

    public List<City> getMockCities() { return mockCities; }

    public List<Country> getMockCountries() { return mockCountries; }

    public List<Role> getMockRoles() { return mockRoles; }

    public List<User> getMockUsers() { return mockUsers; }
}