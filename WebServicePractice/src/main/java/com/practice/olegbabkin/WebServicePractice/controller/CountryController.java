package com.practice.olegbabkin.WebServicePractice.controller;

import java.util.List;

import com.practice.olegbabkin.WebServicePractice.domain.City;
import com.practice.olegbabkin.WebServicePractice.domain.Country;
import com.practice.olegbabkin.WebServicePractice.service.CountryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "api/world/country")
public class CountryController {

    @Autowired
    private CountryService countryService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<?> getAll() {
        return new ResponseEntity<List<Country>>(countryService.getAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{code}", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
    public ResponseEntity<?> getCountryByCode(@PathVariable(value = "code") String code) {
        Country country = countryService.getOne(code);
        if (country != null) {
            return new ResponseEntity<Country>(country, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/continent/{name}", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
    public ResponseEntity<?> getCountriesByContinent(@PathVariable(value = "name") String name) {
        List<Country> countries = countryService.getByContinent(name);
        if (!countries.isEmpty()) {
            return new ResponseEntity<List<Country>>(countries, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @RequestMapping(value = "/{name}/city/all", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
    public ResponseEntity<?> getCitiesByCountry(@PathVariable(value = "name") String name) {
        Country country = countryService.getByName(name);
        if (country != null) {
            List<City> cities = country.getCities();

            if (!cities.isEmpty()) {
                return new ResponseEntity<List<City>>(cities, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/top10/population/lessthan/{value}", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
    public ResponseEntity<?> getTop10ByPopulationLessThan(@PathVariable(value = "value") Integer value) {
        List<Country> countries = countryService.getTop10ByPopulationLessThan(value);
        if (!countries.isEmpty()) {
            return new ResponseEntity<List<Country>>(countries, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @RequestMapping(value = "/{code}/capital", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
    public ResponseEntity<?> getCapital(@PathVariable(value = "code") String code) {
        City capital = countryService.getCountryCapital(code);
        if (capital != null) {
            return new ResponseEntity<City>(capital, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> add(@RequestBody Country country) {
        Country insertedCountry = countryService.saveOne(country);
        return new ResponseEntity<Country>(insertedCountry, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{code}/update", method = RequestMethod.PUT)
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> update(@RequestBody Country country, @PathVariable String code) {
        Country countryGet = countryService.getOne(code);
        if (countryGet != null) {
            country.setCode(code);
            countryGet = countryService.saveOne(country);
            return new ResponseEntity<Country>(countryGet, HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("No country with code: " + code, HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{code}", method = RequestMethod.DELETE)
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<String> delete(@PathVariable String code) {
        Country country = countryService.getOne(code);
        if (country != null) {
            countryService.deleteOne(country);
            return new ResponseEntity<String>("Successfuly deleted!", HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("Nothing to delete!", HttpStatus.NOT_FOUND);
        }
    }
}