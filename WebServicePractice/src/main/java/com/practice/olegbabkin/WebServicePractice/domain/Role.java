package com.practice.olegbabkin.WebServicePractice.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "roles")
public class Role {

  @Id
  @GeneratedValue
  private long id;

  @Column(unique = true)
  private String name;
    
  @ManyToMany(mappedBy = "roles")
  private List<User> users;

  public Role() { }

  public Role(String name) {
    this.name = name;
  }

  public Role(long id, String name) {
    this.id = id;
    this.name = name;
  }

  public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}