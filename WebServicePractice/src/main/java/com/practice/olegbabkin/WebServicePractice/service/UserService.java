package com.practice.olegbabkin.WebServicePractice.service;

import com.practice.olegbabkin.WebServicePractice.domain.User;
import com.practice.olegbabkin.WebServicePractice.repository.UserRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public User findByUsername(String name) {
        return userRepository.findByUsername(name);
    }

    public List<User> findAll(){
        return userRepository.findAll();
    }

    public User addUser(User user) throws Exception  {
        if (findByUsername(user.getUsername())!=null){
            throw new Exception(
              "There is an account with that email address:" + user.getUsername());
        }
        User userTemp = new User();
        userTemp.setUsername(user.getUsername());
        userTemp.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        userTemp.setRoles(null);
        return userRepository.save(userTemp);
    }

	public User findOne(Long id) {
        return userRepository.findOne(id);
    }
    
    public void deleteAll() {
        userRepository.deleteAll();
    }
}