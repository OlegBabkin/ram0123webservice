package com.practice.olegbabkin.WebServicePractice.repository;

import com.practice.olegbabkin.WebServicePractice.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long>{
    User findByUsername(String username);
}