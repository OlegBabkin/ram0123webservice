package com.practice.olegbabkin.WebServicePractice.security;

import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

    // the client application provides this in order to be allowed to send request to the server
    @Value("${security.jwt.client-id}")
    private String clientId;

    /* In a non-trivial implementation client ids and passwords will 
    be securely stored in a database and retrievable through a 
    separate API that clients applications access during deployment. */
    @Value("${security.jwt.client-secret}")
    private String clientSecret;

    // define grant type password here because it’s not enabled by default
    @Value("${security.jwt.grant-type}")
    private String grantType;

    // level of access allowed to resources
    @Value("${security.jwt.scope-read}")
    private String scopeRead;

    // The resource Id specified here must be specified on the resource server as well
    @Value("${security.jwt.scope-write}")
    private String scopeWrite;

    @Value("${security.jwt.resource-ids}")
    private String resourceIds;

    @Autowired
    private TokenStore tokenStore;

    @Autowired
    private JwtAccessTokenConverter accessTokenConverter;

    // hecking user credential validity
    @Autowired
    private AuthenticationManager authenticationManager;

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory().withClient(clientId).secret(clientSecret).authorizedGrantTypes(grantType)
                .scopes(scopeRead, scopeWrite).resourceIds(resourceIds);
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        // enables chaining multiple types of claims containing different information
        TokenEnhancerChain enhancerChain = new TokenEnhancerChain();
        enhancerChain.setTokenEnhancers(Arrays.asList(accessTokenConverter));
        endpoints.tokenStore(tokenStore).accessTokenConverter(accessTokenConverter).tokenEnhancer(enhancerChain)
                .authenticationManager(authenticationManager);
    }

}