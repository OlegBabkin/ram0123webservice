package com.practice.olegbabkin.WebServicePractice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.practice.olegbabkin.WebServicePractice.domain.Role;
import com.practice.olegbabkin.WebServicePractice.repository.RoleRepository;

@Service
public class RoleService {

    @Autowired
    RoleRepository roleRepository;

    public List<Role> findAll() {
        return roleRepository.findAll();
    }

    public Role findOne(Long id) {
        return roleRepository.findOne(id);
    }

    public Role saveOne(Role role) {
        return roleRepository.save(role);
    }

    public void deleteAll() {
        roleRepository.deleteAll();
    }
}