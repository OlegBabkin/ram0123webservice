package com.practice.olegbabkin.WebServicePractice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.practice.olegbabkin.WebServicePractice.domain.City;
import com.practice.olegbabkin.WebServicePractice.domain.Country;
import com.practice.olegbabkin.WebServicePractice.repository.CityRepository;
import com.practice.olegbabkin.WebServicePractice.repository.CountryRepository;
import java.util.List;;

@Service
public class CountryService {

    @Autowired
    private CountryRepository countryRepository;
    
    @Autowired
    private CityRepository cityRepository;

    public Country saveOne(Country country) {
        return countryRepository.save(country);
    }

    public void deleteOne(Country country) {
        countryRepository.delete(country);
    }

    public void deleteOneById(String code) {
        countryRepository.delete(code);
    }

    public void deleteAll() {
        countryRepository.deleteAll();
    }

    public Country getOne(String code) {
        return countryRepository.findByCodeIgnoreCase(code);
    }

    public List<Country> getAll() {
        return countryRepository.findAll();
    }

    public List<Country> getByContinent(String continent) {
        return countryRepository.findByContinentIgnoreCase(continent);
    }

    public List<Country> getTop10ByPopulationLessThan(Integer value) {
        return countryRepository.findTop10ByPopulationLessThanEqualOrderByPopulationDesc(value);
    }

    public Country getByName(String name) {
        return countryRepository.findByNameIgnoreCase(name);
    }

    public City getCountryCapital(String countryCode) {
        Country country = getOne(countryCode);
        if (country == null) return null;
        if (country.getCapital() == null) return null;
        return cityRepository.findOne(country.getCapital());
    }

    public Long getRowsCount() {
        return countryRepository.count();
    }
}