package com.practice.olegbabkin.WebServicePractice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.practice.olegbabkin.WebServicePractice.domain.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
    
}