package com.practice.olegbabkin.WebServicePractice;

/**
 * To build REST-API with JWT authentication:
 * 1. Configure Spring Security
 * 2. Configure Authorization Server
 * 3. Configure Resource Server
 * 4. Configure a Data Source (if needed)
 * 5. Create Database Scripts and Test Data (if needed schema.sql and data.sql)
 * 6. Repositories, Services, RestControllers, Entities
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebServicePracticeApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebServicePracticeApplication.class, args);
	}
}
