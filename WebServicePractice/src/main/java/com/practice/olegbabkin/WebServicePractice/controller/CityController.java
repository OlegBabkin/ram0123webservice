package com.practice.olegbabkin.WebServicePractice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.practice.olegbabkin.WebServicePractice.domain.City;
import com.practice.olegbabkin.WebServicePractice.service.CityService;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "api/world/city")
public class CityController {

    @Autowired
    private CityService cityService;
    
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<City>> getAll() {
        return new ResponseEntity<>(cityService.getAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
    public ResponseEntity<?> getCity(@PathVariable(value = "id") Long id) {
        City city = cityService.getOne(id);
        if (city != null) {
            return new ResponseEntity<City>(city, HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("<p>Not found!</p>", HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/name/part/{part}", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
    public ResponseEntity<?> getCitiesByNameContains(@PathVariable(value = "part") String part) {
        List<City> cities = cityService.getCitiesByNameContains(part);
        if (!cities.isEmpty()) {
            return new ResponseEntity<List<City>>(cities, HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("<p>No cities or wrong name!</p>", HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> add(@RequestBody City city) {
        City insertedCity = cityService.saveOne(city);
        return new ResponseEntity<City>(insertedCity, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}/update", method = RequestMethod.PUT)
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> update(@RequestBody City city, @PathVariable Long id) {
        City cityGet = cityService.getOne(id);
        if (cityGet != null) {
            city.setId(id);
            cityGet = cityService.saveOne(city);
            return new ResponseEntity<City>(cityGet, HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("No city with id: " + id, HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        City city = cityService.getOne(id);
        if (city != null) {
            cityService.deleteOne(city);
            return new ResponseEntity<String>("Successfuly deleted!", HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("Nothing to delete!", HttpStatus.NOT_FOUND);
        }
    }
}