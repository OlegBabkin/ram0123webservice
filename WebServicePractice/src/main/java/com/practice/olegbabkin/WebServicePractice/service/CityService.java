package com.practice.olegbabkin.WebServicePractice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.practice.olegbabkin.WebServicePractice.domain.City;
import com.practice.olegbabkin.WebServicePractice.repository.CityRepository;
import java.util.List;

@Service
public class CityService {

    @Autowired
    private CityRepository cityRepository;

    public City saveOne(City city) {
        return cityRepository.save(city);
    }

    public void deleteOne(City city) {
        cityRepository.delete(city);
    }

    public void deleteOneById(Long id) {
        cityRepository.delete(id);
    }

    public void deleteAll() {
        cityRepository.deleteAll();
    }

    public City getOne(Long id) {
        return cityRepository.findOne(id);
    }

    public List<City> getAll() {
        return cityRepository.findAll();
    }

    public List<City> getCitiesByNameContains(String part) {
        return cityRepository.findByNameContainingIgnoreCase(part);
    }

    public Long getRowsCount() {
        return cityRepository.count();
    }
}