package com.practice.olegbabkin.WebServicePractice.repository;

import com.practice.olegbabkin.WebServicePractice.domain.Country;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<Country, String> {

    Country findByCodeIgnoreCase(String code);

    Country findByNameIgnoreCase(String name);

    List<Country> findByContinentIgnoreCase(String continent);

    List<Country> findTop10ByPopulationLessThanEqualOrderByPopulationDesc(Integer value);

}