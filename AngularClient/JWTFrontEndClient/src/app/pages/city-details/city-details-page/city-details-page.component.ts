import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { City } from '../../../model/model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-city-details-page',
  exportAs: 'modal',
  templateUrl: './city-details-page.component.html',
  styleUrls: ['./city-details-page.component.scss']
})
export class CityDetailsPageComponent implements OnInit {

  @Input() city: City;
  @Output() modalClose : EventEmitter<any> = new EventEmitter<any>();

  constructor(private router : Router) { }

  ngOnInit() {
  }

  closeModal($event) {
    this.router.navigate([{outlets: {modal: null}}]);
    this.modalClose.next($event);
  }

}
