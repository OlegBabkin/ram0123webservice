import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CityDetailsRoutingModule } from './city-details-routing.module';
import { CityDetailsPageComponent } from './city-details-page/city-details-page.component';
import { HeaderModule } from '../../general-components/header/header.module';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

@NgModule({
  imports: [
    CommonModule,
    HeaderModule,
    CityDetailsRoutingModule,
    MDBBootstrapModule.forRoot()
  ],
  declarations: [CityDetailsPageComponent]
})
export class CityDetailsModule { }
