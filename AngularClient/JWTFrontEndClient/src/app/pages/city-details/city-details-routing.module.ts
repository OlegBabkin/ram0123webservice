import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CityDetailsPageComponent } from './city-details-page/city-details-page.component';
import { UserGuard } from '../../guards/user.guard';
import { AdminGuard } from '../../guards/admin.guard';

const routes: Routes = [
  {
    path: 'detail',
    component: CityDetailsPageComponent,
    outlet: 'modal',
    canActivate: [UserGuard, AdminGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CityDetailsRoutingModule { }
