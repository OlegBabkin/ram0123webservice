import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Country } from '../../../../model/model';
import { Debugable } from '../../../../helpers/debugable';
import { RestService } from '../../../../services/rest.service';

@Component({
  selector: 'app-countries-table',
  templateUrl: './countries-table.component.html',
  styleUrls: ['./countries-table.component.scss']
})
export class CountriesTableComponent extends Debugable implements AfterViewInit {

  columns = ['code', 'name', 'continent', 'region', 'headofstate', 'population', 'governmentform'];
  dataSource: MatTableDataSource<Country>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private rest: RestService) {
    super(CountriesTableComponent.name);
    this.dataSource = new MatTableDataSource();
    this.getCountries();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getCountries() {
    this.rest.getCountries().subscribe(
      res => {
        this.log('Countries loaded');
        this.dataSource.data = res;
      },
      err => console.log(err)
    );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim().toLowerCase();
    this.dataSource.filter = filterValue;
  }

}
