import { RestService } from './../../../../services/rest.service';
import { Debugable } from './../../../../helpers/debugable';
import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { City } from '../../../../model/model';

@Component({
  selector: 'app-cities-table',
  templateUrl: './cities-table.component.html',
  styleUrls: ['./cities-table.component.scss']
})
export class CitiesTableComponent extends Debugable implements AfterViewInit {

  columns = ['id', 'name', 'country', 'countrycode', 'district', 'population'];
  dataSource: MatTableDataSource<City>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private rest: RestService) {
    super(CitiesTableComponent.name);
    this.dataSource = new MatTableDataSource();
    this.getCities();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getCities() {
    this.rest.getCities().subscribe(
      res => {
        this.log('Cities loaded');
        this.dataSource.data = res;
      },
      err => console.log(err)
    );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim().toLowerCase();
    this.dataSource.filter = filterValue;
  }

}
