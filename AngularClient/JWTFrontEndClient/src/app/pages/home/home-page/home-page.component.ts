import { City } from './../../../model/model';
import { Component, OnInit } from '@angular/core';
import { Debugable } from '../../../helpers/debugable';
import { RestService } from '../../../services/rest.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent extends Debugable implements OnInit {

  isCitiesSelected = false;
  title = 'Countries and cities overview';
  description = 'There you can find basic information about countries and cities. If signed in, you can look more detailed information.';

  constructor(private rest: RestService) { super(HomePageComponent.name); }

  ngOnInit() {

  }

  onTabChange(event) {
    this.log(event.tab.textLabel + 'tab selected');
    this.isCitiesSelected = true; // (event.tab.textLabel === 'Cities' ? true : false);
  }

}
