import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { 
  MatFormFieldModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatInputModule,
  MatTabsModule
} from '@angular/material';

import { HomeRoutingModule } from './home-routing.module';
import { HomePageComponent } from './home-page/home-page.component';
import { CitiesTableComponent } from './home-partials/cities-table/cities-table.component';
import { CountriesTableComponent } from './home-partials/countries-table/countries-table.component';
import { HeaderModule } from '../../general-components/header/header.module';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    MatFormFieldModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatTabsModule,
    HeaderModule
  ],
  // exports: [
  //   MatFormFieldModule,
  //   MatTableModule,
  //   MatPaginatorModule,
  //   MatSortModule
  // ],
  declarations: [
    HomePageComponent,
    CitiesTableComponent,
    CountriesTableComponent
  ]
})
export class HomeModule { }
