import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CountryDetailsRoutingModule } from './country-details-routing.module';
import { CountryDetailsPageComponent } from './country-details-page/country-details-page.component';

@NgModule({
  imports: [
    CommonModule,
    CountryDetailsRoutingModule
  ],
  declarations: [CountryDetailsPageComponent]
})
export class CountryDetailsModule { }
