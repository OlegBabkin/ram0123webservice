import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Debugable } from '../../../helpers/debugable';
import { Country, City } from '../../../model/model';
import { RestService } from '../../../services/rest.service';
import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';

@Component({
  selector: 'app-add-city',
  templateUrl: './add-city.component.html',
  styleUrls: ['./add-city.component.scss']
})
export class AddCityComponent extends Debugable implements OnInit {

  cityForm: FormGroup;
  countries: Country[];
  filteredCountries: Observable<any[]>;
  country: Country;
  editing: boolean = false;
  cityId: number;

  constructor(public formBuilder: FormBuilder, public rest: RestService, public router: Router, public route: ActivatedRoute) {
    super(AddCityComponent.name);

    this.cityForm = this.formBuilder.group({
      id: ['', [Validators.required, Validators.pattern('^[0-9\.]+$')]],
      name: ['', [Validators.required]],
      country: new FormControl(new Country(), Validators.required),
      district: '',
      population: ['', [Validators.pattern('^[0-9\.]+$')]],
    });

    this.getCountries();
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => this.cityId = params['id']);
    this.editing = this.cityId != undefined;
    let city: City;
    if (this.editing) {
      this.rest.getCityById(this.cityId).subscribe(res => {
        this.log(JSON.stringify(res));
        this.cityForm.setValue(res);
      }, err => console.log(err));
    }
  }

  getCountries() {
    this.rest.getCountries().subscribe(
      res => {
        this.countries = res;
        this.filteredCountries = this.cityForm.controls['country'].valueChanges
          .pipe(
            startWith(this.cityForm.get('country').value),
            map(val => this.displayFn(val)),
            map(name => this.filterCountries(name))
          );
      },
      err => console.log(err)
    );
  }

  filterCountries(name: string) {
    const n = name === undefined ? '' : name.toLowerCase()
    return this.countries.filter(c => c.name
      .toLowerCase().startsWith(n));
  }

  displayFn(value: any): string {
    return value && typeof value === 'object' ? value.name : value;
  }

  submit() {
    if (this.cityForm.valid) {
      if (this.editing) {
        this.log('editing: ' + this.editing);
        this.rest.udateCity(this.cityForm.value).subscribe(res => {
          this.router.navigate(['./cities']);
          this.log('City updated!');
          return true;
        }, error => {
          this.router.navigate(['./cities']);
          console.error("Error updating city!");
          return Observable.throw(error);
        });
      } else {
        this.rest.saveCity(this.cityForm.value).subscribe(res => {
          this.router.navigate(['./cities']);
          this.log('City added!');
          return true;
        }, error => {
          this.router.navigate(['./cities']);
          console.error("Error saving city!");
          return Observable.throw(error);
        });
      }
    }
  }
}
