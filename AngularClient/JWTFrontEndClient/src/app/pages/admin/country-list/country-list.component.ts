import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource, MatSort } from '@angular/material';
import { Country } from '../../../model/model';
import { RestService } from '../../../services/rest.service';
import { Debugable } from '../../../helpers/debugable';

@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.scss']
})
export class CountryListComponent extends Debugable implements AfterViewInit {
  
  dataSource: MatTableDataSource<Country>;
  columns = [ 'code', 'name', 'population', 'actions' ];
  excludeColumns = [ 'actions' ];

  @ViewChild('countryTableSort') countryTableSort: MatSort;

  constructor(private rest: RestService) {
    super(CountryListComponent.name);
    this.dataSource = new MatTableDataSource();
    this.getCountries();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.countryTableSort;
  }

  getCountries() {
    this.rest.getCountries().subscribe(
      res => {
        this.log('Countries loaded');
        this.dataSource.data = res;
      },
      err => console.log(err)
    );
  }

  edit(code) {
    this.log(code);
  }

  delete(code) {
    this.log(code);
  }

}
