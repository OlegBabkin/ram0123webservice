import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { HeaderModule } from '../../general-components/header/header.module';
import { MatTabsModule, MatFormFieldModule, MatSelectModule, MatAutocompleteModule, MatButtonModule } from '@angular/material';

import { AdminPanelComponent } from './admin-panel.component';
import { CountryListComponent } from './country-list/country-list.component';
import { AddCountryComponent } from './add-country/add-country.component';
import { AddCityComponent } from './add-city/add-city.component';
import { CityListComponent } from './city-list/city-list.component';
import { TableModule } from '../../general-components/table/table.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    HeaderModule,
    MatTabsModule,
    MatFormFieldModule,
    TableModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    AdminRoutingModule
  ],
  declarations: [
    AdminPanelComponent,
    CountryListComponent,
    AddCountryComponent,
    AddCityComponent,
    CityListComponent
  ]
})
export class AdminModule { }
