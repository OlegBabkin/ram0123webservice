import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.scss']
})
export class AdminPanelComponent implements OnInit {

  title = 'Administrator panel';
  description = 'Here you can manage countries and cities, of course, if you have administrative rights';
  navLinks = [
    {
      label: 'Manage countries',
      path: './countries',
    },

    {
      label: 'Manage cities',
      path: './cities',
    },

    {
      label: 'Add/Update city',
      path: './cities/add',
    },
  ];

  constructor(private router: Router) { }

  ngOnInit() { }

}
