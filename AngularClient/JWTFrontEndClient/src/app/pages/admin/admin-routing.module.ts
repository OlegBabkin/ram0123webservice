import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminPanelComponent } from './admin-panel.component';
import { CountryListComponent } from './country-list/country-list.component';
import { CityListComponent } from './city-list/city-list.component';
import { AdminGuard } from '../../guards/admin.guard';
import { AddCityComponent } from './add-city/add-city.component';

const adminRoutes: Routes = [
  {
    path: 'admin',
    component: AdminPanelComponent,
    canActivate: [AdminGuard],
    children: [
      {
        path: '',
        canActivateChild: [AdminGuard],
        children: [
          { path: 'countries', component: CountryListComponent },
          { path: 'cities', component: CityListComponent },
          { path: 'cities/add', component: AddCityComponent },
          { path: 'cities/add/:id', component: AddCityComponent },
          { path: '', redirectTo: '/admin/countries', pathMatch: 'full' }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
