import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { Debugable } from '../../../helpers/debugable';
import { MatTableDataSource, MatSort } from '@angular/material';
import { City } from '../../../model/model';
import { RestService } from '../../../services/rest.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-city-list',
  templateUrl: './city-list.component.html',
  styleUrls: ['./city-list.component.scss']
})
export class CityListComponent extends Debugable implements AfterViewInit {
  
  dataSource: MatTableDataSource<City>;
  columns = [ 'id', 'name', 'country', 'countrycode', 'actions' ];
  excludeColumns = [ 'actions', 'country', 'countrycode' ];
  cityId: number;

  @ViewChild('cityTableSort') cityTableSort: MatSort;

  constructor(private rest: RestService) {
    super(CityListComponent.name);
    this.dataSource = new MatTableDataSource();
    this.getCities();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.cityTableSort;
  }

  getCities() {
    this.rest.getCities().subscribe(
      res => {
        this.log('Cities loaded');
        this.dataSource.data = res;
      },
      err => console.log(err)
    );
  }

  edit(id) {
    this.log(id);
    this.cityId = id;
  }

  delete(id: number) {
    this.log(id.toString());
    this.rest.deleteCity(id).subscribe(res => {
      this.log('Deleted city: ' + res);
      this.getCities();
      return true;
    }, err => {
      console.error('Error deleting city!');
      this.getCities();
      return Observable.throw(err);
    });
  }

}
