import { Component, OnInit } from '@angular/core';
import { Debugable } from '../../../helpers/debugable';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent extends Debugable implements OnInit {

  loginForm: FormGroup;
  redirectUrl: string;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private userService: UserService,
    private formBuilder: FormBuilder) {

      super(LoginPageComponent.name);

      this.redirectUrl = this.activatedRoute.snapshot.queryParams['redirectTo'];
      this.loginForm = this.formBuilder.group({
        email: ['', [Validators.required, Validators.pattern('[^ @]*@[^ @]*')]],
        pass: ['', [Validators.required, Validators.minLength(6)]]
      });
  }

  ngOnInit() {
    this.userService.logout();
    this.loginForm.reset();
  }

  login() {
    this.log('email: ' + this.loginForm.controls['email'].value);
    this.log('pass: ' + this.loginForm.controls['pass'].value);

    this.authService.login(this.loginForm.controls['email'].value, this.loginForm.controls['pass'].value)
    .subscribe(
      token => {
        if (token) {
          this.userService.login(token);
          this.navigateAfterSuccess();
        }
      },
      error => {
        console.log('ERROR: ' + JSON.stringify(error));
      }
    );
  }

  private navigateAfterSuccess() {
    if (this.redirectUrl) {
      this.router.navigateByUrl(this.redirectUrl);
    } else {
      this.router.navigate(['']);
    }
  }

}
