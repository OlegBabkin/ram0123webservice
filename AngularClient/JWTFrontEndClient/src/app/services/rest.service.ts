import { City, Country } from './../model/model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class RestService {

  constructor(private http: HttpClient) { }

  getCities(): Observable<City[]> {
    return this.http.get<City[]>('/api/world/city/all');
  }

  getCityById(id: number): Observable<City> {
    return this.http.get<City>(`${'/api/world/city'}/${id}`);
  }

  getCountries(): Observable<Country[]> {
    return this.http.get<Country[]>('/api/world/country/all');
  }

  deleteCity(id: number): Observable<City> {
    return this.http.delete<City>(`${'/api/world/city'}/${id}`);
  }

  saveCity(city: City): Observable<City> {
    let body = JSON.stringify(city);
    return this.http.post<City>('/api/world/city/add', body, httpOptions);
  }

  udateCity(city: City): Observable<City> {
    let body = JSON.stringify(city);
    return this.http.put<City>(`${'/api/world/city'}/${city.id}/update`, body, httpOptions);
  }
}
