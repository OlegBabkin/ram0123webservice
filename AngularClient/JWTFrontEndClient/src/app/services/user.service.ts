import { Injectable } from '@angular/core';
import { JwtHelper, tokenNotExpired } from 'angular2-jwt';

import { Debugable } from '../helpers/debugable';
import { TOKEN_NAME } from './auth.constants';

@Injectable()
export class UserService extends Debugable {
  private jwtHelper: JwtHelper = new JwtHelper();
  private accessToken: string;
  private isAdmin = false;

  constructor() { super(UserService.name); }

  login(accessToken: string) {
    const decodedToken = this.jwtHelper.decodeToken(accessToken);
    this.log('Decoded token: ' + decodedToken);

    this.isAdmin = decodedToken.authorities.some(a => a === 'ADMIN');
    this.log('Athority: ' + this.isAdmin ? 'Administrator' : 'User');
    this.setToken(accessToken);
  }

  logout() {
    this.accessToken = null;
    this.isAdmin = false;
    this.removeToken();
  }

  isAdminAuth(): boolean {
    return this.isAdmin;
  }

  isUserAuth(): boolean {
    return this.accessToken && !this.isAdmin;
  }

  getToken(): string {
    return localStorage.getItem(TOKEN_NAME);
  }

  setToken(token: string) {
    this.accessToken = token;
    localStorage.setItem(TOKEN_NAME, token);
  }

  removeToken() {
    localStorage.removeItem(TOKEN_NAME);
  }

  isAuthenticated(): boolean {
    const token = this.getToken();
    return tokenNotExpired(TOKEN_NAME, token);
  }

}
