import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';

import { AUTH_CLIENT_ID, AUTH_CLIENT_SECRET, DEBUG, TOKEN_NAME } from '../services/auth.constants';
import { Debugable } from '../helpers/debugable';
import { tokenNotExpired } from 'angular2-jwt';

@Injectable()
export class AuthService extends Debugable {

  constructor(private http: HttpClient) {
    super(AuthService.name);
  }

  login(username: string, password: string) {
    const encUsername = encodeURIComponent(username);
    const encPassword = encodeURIComponent(password);
    const body = 'username=' + encUsername + '&password=' + encPassword + '&grant_type=password';

    const options = {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .set('Authorization', 'Basic ' + btoa(AUTH_CLIENT_ID + ':' + AUTH_CLIENT_SECRET))
    };

    this.log('Authorization: ' + options.headers.get('Authorization'));
    this.log('Content-Type: ' + options.headers.get('Content-Type'));

    return this.http.post('/oauth/token', body, options).map((res: any) => {
      if (res.access_token) {
        this.log('Token: ' + res.access_token);
        return res.access_token;
      }
      return null;
    });
  }

}
