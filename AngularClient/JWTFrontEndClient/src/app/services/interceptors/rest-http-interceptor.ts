import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Debugable } from '../../helpers/debugable';
import { AuthService } from '../auth.service';
import { UserService } from '../user.service';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';

const excludedUrl = [
    '/api/world/city/all',
    '/api/world/country/all',
    '/oauth/token'
];

@Injectable()
export class RestHttpInterceptor extends Debugable implements HttpInterceptor {

    constructor(private userService: UserService, private router: Router) { super(RestHttpInterceptor.name); }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.log('intercepted request: ' + req.url);

        if (excludedUrl.some(x => x === req.url)) {
            this.log('Excluded request: ' + req.url);
            return next.handle(req);
        } else {
            const token = this.userService.getToken();
            const authReq = req.clone({
                setHeaders: { Authorization: 'Bearer ' + token }
            });
            this.log(authReq.headers.getAll('Authorization').toString());

            return next.handle(authReq).catch((error, caught) => {
                if (error.status === 401 || error.status === 403) {
                    console.log('ERROR: Not authorized');
                    this.router.navigate(['/login']);
                    return Observable.throw(error);
                } else {
                    console.log('ERROR: ');
                    console.log(error);
                    return Observable.throw(error);
                }
            }) as any;
        }
    }
}
