import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminGuard } from './guards/admin.guard';

const routes: Routes = [

  {
    path: '',
    loadChildren: 'app/pages/home/home.module#HomeModule'
  },

  {
    path: '',
    loadChildren: 'app/pages/login/login.module#LoginModule'
  },

  {
    path: 'admin',
    loadChildren: 'app/pages/admin/admin.module#AdminModule',
    canLoad: [AdminGuard]
  },

  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes
    )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
