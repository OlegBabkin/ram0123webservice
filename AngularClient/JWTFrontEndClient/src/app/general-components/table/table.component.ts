import {
  Component,
  OnInit,
  Input,
  ViewChild,
  AfterViewInit,
  EventEmitter,
  ContentChildren,
  QueryList,
  AfterContentInit
} from '@angular/core';

import {
  MatTableDataSource,
  MatPaginator,
  MatSort,
  MatColumnDef,
  MatTable
} from '@angular/material';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent<T> implements OnInit, AfterViewInit, AfterContentInit {

  @ContentChildren(MatColumnDef) columnDefs: QueryList<MatColumnDef>;

  @ViewChild(MatTable) table: MatTable<T>;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  @Input() columns: string[];
  @Input() excludedColumns: string[];
  @Input() dataSource: MatTableDataSource<T>;

  constructor() { }

  ngOnInit() { }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngAfterContentInit() {
    this.columnDefs.forEach(columnDef => this.table.addColumnDef(columnDef));
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim().toLowerCase();
    this.dataSource.filter = filterValue;
  }

  excludedColumn(column: string): boolean {
    return this.excludedColumns.some(c => c === column);
  }

}
