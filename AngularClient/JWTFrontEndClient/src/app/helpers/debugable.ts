import { DEBUG } from "../services/auth.constants";

export class Debugable {
    tag = '';

    constructor(tag: string) {
        this.tag = '-- ' + tag + ' --:';
    }

    log(body: string) {
        if (DEBUG) {
            console.log(this.tag, body);
        }
    }
}