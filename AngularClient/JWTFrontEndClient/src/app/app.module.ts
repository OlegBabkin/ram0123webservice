import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { AppRoutingModule } from './app-routing.module';
import { NgHttpLoaderModule } from 'ng-http-loader/ng-http-loader.module';
import { AdminModule } from './pages/admin/admin.module';

import { AppComponent } from './app.component';

import { NavModule } from './general-components/nav/nav.module';

import { UserService } from './services/user.service';
import { AuthService } from './services/auth.service';
import { UserGuard } from './guards/user.guard';
import { AdminGuard } from './guards/admin.guard';
import { RestService } from './services/rest.service';
import { RestHttpInterceptor } from './services/interceptors/rest-http-interceptor';

@NgModule({
  declarations: [ AppComponent ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgHttpLoaderModule,
    NavModule,
    AdminModule,
    AppRoutingModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RestHttpInterceptor,
      multi: true
    },
    UserService,
    AuthService,
    UserGuard,
    AdminGuard,
    RestService
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
