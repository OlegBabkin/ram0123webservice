export class Country {

    code: string;
    name: string;
    continent: string;
    region: string;
    surfacearea: number;
    indepyear: number;
    population: number;
    lifeexpectancy: number;
    gnp: number;
    gnpold: number;
    localname: string;
    governmentform: string;
    headofstate: string;
    capital: number;
    code2: string;

    constructor() { }
}

export class City {

    id: number;
    name: string;
    country: Country;
    district: string;
    population: number;

    constructor() { }
}
